package com.mvp.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.shopping.R;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 隐藏标题栏
		// 使用菜单栏目时候用 // 原先版本--
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);// 全屏 放在创建视图前面
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);// 隐藏状态栏
		setContentView(R.layout.activity_splash);
		Thread myThread = new Thread() {// 创建子线程
			@Override
			public void run() {
				try {
					// 可以用网络请求做一些逻辑操作
					// https://blog.csdn.net/zone_/article/details/66476733
					sleep(3000);// 使程序休眠三秒
					Intent it = new Intent(getApplicationContext(),
							MainActivity.class);// 启动MainActivity
					startActivity(it);
					finish();// 关闭当前活动
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		myThread.start();// 启动线程
	}
}
