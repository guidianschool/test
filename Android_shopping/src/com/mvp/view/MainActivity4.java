package com.mvp.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.example.shopping.R;
import com.mob.MobSDK;
import com.mvp.model.Quanju;
import com.mvp.model.User;
import com.mvp.model.Version;
import com.mvp.model.response.ResponseData;
import com.mvp.unitl.HttpGetPostRequest;
import com.mvp.unitl.SelfDialog;
import com.mvp.unitl.imageViewHelp.ImageViewHelp;
import com.mvp.unitl.updateSoftware.UpdateManager;
import com.mvp.unitl.updateSoftware.UpdateService;

public class MainActivity4 extends Activity {
	// -----------自定义变量-----------
	Version serverVersion = new Version();// 服务器版本信息
	Quanju q; // 定义全局类
	// 底部菜单栏控件
	private LinearLayout layout_menu_1, layout_menu_2, layout_menu_3,
			layout_menu_4;
	// 订单状态控件
	private LinearLayout layout_btn_1, layout_btn_2, layout_btn_3,
			layout_btn_4;
	Intent intent;// 每次用时一定要初始化
	private ImageView imagv_1, imagv_2, imagv_3, imagv_4;
	private TextView textV_1, textV_2, textV_3, textV_4, user_msg_name1,
			user_msg_name2, login_state;
	private ImageView user_msg_header;
	private User user = new User();
	String loginurl = "http://jintime.club:8080/static/images/goods/malafen3.jpg";
	Context context = this;
	// 在消息队列中实现对控件的更改-单个图片异步加载处理方法
	@SuppressLint("HandlerLeak")
	private Handler handle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				Bitmap bmp = (Bitmap) msg.obj;
				if (bmp != null) {
					user_msg_header.setImageBitmap(bmp);
				} else {
					user_msg_header.setImageResource(R.drawable.header_pic2);
				}
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity4);
		// 底部导航跳转页面方法
		routerPageFun();
		// 获取所有表数据
		q = (Quanju) getApplicationContext();
		// 当前用户信息初始化
		changeUserInfo();
		// 订单状态控件初始化
		noticeInit();

	}

	/**
	 * 订单状态控件初始化
	 */
	private void noticeInit() {
		layout_btn_1 = (LinearLayout) findViewById(R.id.a_1);
		layout_btn_2 = (LinearLayout) findViewById(R.id.a_2);
		layout_btn_3 = (LinearLayout) findViewById(R.id.a_3);
		layout_btn_4 = (LinearLayout) findViewById(R.id.a_4);
		/**
		 * 已发货事件
		 */
		layout_btn_3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showShare();
			}

		});

		/**
		 * 待收货事件
		 */
		layout_btn_2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 实现通知栏通知效果
				// 获取服务器和本地版本号
				initGlobal();
				// 主线程延迟一秒 获取链接先在做下面操作
				SystemClock.sleep(1000);
				// 确认是否升级--通知栏方式
				checkVersion();
				// 确认是否升级2--弹出窗方式
				// CommonUpdateApp();
			}

		});
		/**
		 * 代付款事件
		 */
		layout_btn_1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 实现通知栏通知效果
				showNotifyWithVibrate();
			}

			/**
			 * 通知栏:图标+标题+内容
			 */
			@SuppressWarnings("unused")
			private void notificationText() {
				NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
						context)
				// 设置小图标
						.setSmallIcon(R.drawable.logo2)
						// 设置通知标题
						.setContentTitle("测试中…………")
						// 设置通知内容
						.setContentText("测试notification中的使用包括图标，内容，时间等")
						// 设置通知时间，默认为系统发出通知的时间，通常不用设置
						.setWhen(System.currentTimeMillis());
				Notification notify = mBuilder.build();
				mNotificationManager.notify(1, notify);
			}

			/**
			 * 通知栏:图标+标题+内容+铃声 展示有自定义铃声效果的通知
			 */
			@SuppressWarnings("unused")
			private void showNotifyWithRing() {
				NotificationManager mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				NotificationCompat.Builder builder = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.logo2)
						.setContentTitle("我是伴有铃声效果的通知")
						.setContentText("美妙么?安静听~")
						// 调用系统默认响铃,设置此属性后setSound()会无效
						// .setDefaults(Notification.DEFAULT_SOUND)
						// 调用自己提供的铃声，位于 /res/values/raw 目录下
						.setSound(
								Uri.parse("android.resource://com.example.shopping/"
										+ R.drawable.aa));
				// 另一种设置铃声的方法
				// Notification notify = builder.build();
				// 调用系统默认铃声
				// notify.defaults = Notification.DEFAULT_SOUND;
				// 调用自己提供的铃声
				// notify.sound =
				// Uri.parse("android.resource://com.littlejie.notification/"+R.raw.sound);
				// 调用系统自带的铃声
				// notify.sound =
				// Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI,"2");
				// mManager.notify(2,notify);
				// 设跳转页面
				Intent intent = new Intent(context, MainActivity.class);
				PendingIntent pendingIntent = PendingIntent.getActivity(
						context, 0, intent, 0);
				builder.setContentIntent(pendingIntent);
				mManager.notify(2, builder.build());
			}

			/**
			 * 通知栏:图标+标题+内容+震动 |展示有震动效果的通知,需要在AndroidManifest.xml中申请震动权限
			 * <uses-permission android:name="android.permission.VIBRATE" />
			 * 补充:测试震动的时候,手机的模式一定要调成铃声+震动模式,否则你是感受不到震动的
			 */
			@SuppressWarnings("unused")
			private void showNotifyWithVibrate() {
				// 系统级的Service
				NotificationManager mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				// 自定义震动效果
				long[] vibrate = new long[] { 0, 500, 1000, 1500 };
				NotificationCompat.Builder builder = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.logo2)
						.setContentTitle("我是伴有震动效果的通知")
						.setContentText("颤抖吧,凡人~")
						// 使用系统默认的震动参数,会与自定义的冲突
						// .setDefaults(Notification.DEFAULT_VIBRATE)
						// 自定义震动效果
						.setVibrate(vibrate);
				// 另一种设置震动的方法
				// Notification notify = builder.build();
				// 调用系统默认震动
				// notify.defaults = Notification.DEFAULT_VIBRATE;
				// 调用自己设置的震动
				// notify.vibrate = vibrate;
				// mManager.notify(3,notify);
				// 设跳转页面
				Intent intent = new Intent(context, MainActivity.class);
				PendingIntent pendingIntent = PendingIntent.getActivity(
						context, 0, intent, 0);
				builder.setContentIntent(pendingIntent);
				mManager.notify(3, builder.build());
			}

			/**
			 * 通知栏:图标+标题+内容+闪光 显示带有呼吸灯效果的通知
			 */
			@SuppressWarnings("unused")
			private void showNotifyWithLights() {
				NotificationManager mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				final NotificationCompat.Builder builder = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.logo2)
						.setContentTitle("呼吸灯效果的通知").setContentText("一闪一闪~")
						// ledARGB 表示灯光颜色、 ledOnMS 亮持续时间、ledOffMS 暗的时间
						.setLights(0xFF0000, 3000, 3000);
				Notification notify = builder.build();
				// 只有在设置了标志符Flags为Notification.FLAG_SHOW_LIGHTS的时候，才支持呼吸灯提醒。
				notify.flags = Notification.FLAG_SHOW_LIGHTS;
				// 设置lights参数的另一种方式
				// notify.ledARGB = 0xFF0000;
				// notify.ledOnMS = 500;
				// notify.ledOffMS = 5000;
				mManager.notify(4, builder.build());
			}

		});

	}

	// 底部菜单相关 不确定
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity4, menu);
		return true;
	}

	// 底部导航跳转页面方法
	public void routerPageFun() {
		layout_menu_1 = (LinearLayout) findViewById(R.id.layout_menu_1);
		layout_menu_2 = (LinearLayout) findViewById(R.id.layout_menu_2);
		layout_menu_3 = (LinearLayout) findViewById(R.id.layout_menu_3);
		layout_menu_4 = (LinearLayout) findViewById(R.id.layout_menu_4);
		imagv_4 = (ImageView) findViewById(R.id.imagev_4);
		imagv_4.setImageResource(R.drawable.center4_2);

		textV_4 = (TextView) findViewById(R.id.textV_4);
		textV_4.setTextColor(getResources().getColor(R.color.text_bg));

		layout_menu_2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(MainActivity4.this, MainActivity2.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				finish();// 会清理当前的页面记录
			}
		});
		layout_menu_3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(MainActivity4.this, MainActivity3.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				finish();
			}
		});
		layout_menu_1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(MainActivity4.this, MainActivity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				finish();
			}
		});

	}

	// 改变用户信息
	public void changeUserInfo() {

		user_msg_name1 = (TextView) MainActivity4.this
				.findViewById(R.id.user_msg_name1);
		user_msg_name2 = (TextView) MainActivity4.this
				.findViewById(R.id.user_msg_name2);
		login_state = (TextView) MainActivity4.this
				.findViewById(R.id.login_state);
		user_msg_header = (ImageView) MainActivity4.this
				.findViewById(R.id.user_msg_header);
		user_msg_header.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent();
				// 修改头像
				intent.setClass(MainActivity4.this, UploadpicActivity.class);
				startActivity(intent);
			}
		});

		// 用户未登录时
		if (q.currentUser == null) {
			user_msg_name1.setText("请登录");
			user_msg_name2.setText("登录体验更优");
			login_state.setText("登录    >");
			// 异步加载图片
			ImageViewHelp.loadImage(context, user_msg_header, loginurl);
			// 新建线程加载图片信息，发送到消息队列中 方法1
			// new Thread() {
			// public void run() {
			// String url =
			// "http://jintime.club:8080/static/images/goods/malafen3.jpg";
			// Bitmap bmp = HelpImageView.getURLimage(url);
			// Message msg = new Message();
			// msg.what = 0;
			// msg.obj = bmp;
			// handle.sendMessage(msg);
			// }
			// }.start();
			login_state.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), "请登录", 1).show();
					intent = new Intent();
					intent.setClass(MainActivity4.this, Login.class);
					startActivity(intent);
				}
			});

		} else {
			user_msg_name1.setText(user.getName().toString());
			user_msg_name2.setText("二哈也快乐");
			login_state.setText("签到    >");
			user_msg_header.setImageResource(R.drawable.header_pic);
		}

	}

	/**
	 * 初始化全局变量 实际工作中这个方法中serverVersion从服务器端获取，最好在启动画面的activity中执行
	 */
	public void initGlobal() {
		try {
			serverVersion.localVersion = getPackageManager().getPackageInfo(
					getPackageName(), 0).versionCode; // 设置本地版本号
			new Thread() {
				@Override
				public void run() {
					String url = q.getVersionUrl;
					String param = "name=" + q.appName;
					ResponseData r = HttpGetPostRequest
							.GetData2(HttpGetPostRequest.sendGet(url, param));
					if (r != null && r.Code == 1) // 成功操作
					{
						Version version = HttpGetPostRequest
								.paraseVersion(r.Models.toString());
						if (version.id > 0) {
							serverVersion.id = version.id;
							serverVersion.content = version.content;
							serverVersion.name = version.name;
							serverVersion.url = version.url;
							serverVersion.version = version.version;
						}
					}
				}
			}.start();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 检查更新版本
	 */
	// 自定义弹窗
	SelfDialog selfDialog;

	@SuppressLint("NewApi")
	public void checkVersion() {
		if (serverVersion.id > 0
				&& serverVersion.localVersion < serverVersion.version) {
			StringBuffer Message = new StringBuffer();
			// 发现新版本，提示用户更新 // 构造对话框
			selfDialog = new SelfDialog(context);
			selfDialog.setTitle("软件更新");
			if (!serverVersion.content.isEmpty()) {
				Message.append(serverVersion.content);
				Message.insert(0, "检测到新版本,立即更新吗\n");// ="检测到新版本,立即更新吗\n"+Message;
			} else {
				Message.append("检测到新版本,立即更新吗");
			}
			selfDialog.setMessage(Message.toString().replace("\\n", "\n"));
			selfDialog.setYesOnclickListener("立即更新",
					new SelfDialog.onYesOnclickListener() {
						@SuppressLint("NewApi")
						@Override
						public void onYesClick() {
							selfDialog.dismiss();
							// 显示下载通知栏对话框
							// 开启更新服务UpdateService
							// 这里为了把update更好模块化，可以传一些updateService依赖的值
							// 如布局ID，资源ID，动态获取的标题,这里以app_name为例
							Intent updateIntent = new Intent(context,
									UpdateService.class);
							updateIntent.putExtra("app_name",
									serverVersion.name);
							updateIntent.putExtra("downurl", serverVersion.url);
							startService(updateIntent);
						}
					});
			selfDialog.setNoOnclickListener("暂不更新",
					new SelfDialog.onNoOnclickListener() {
						@Override
						public void onNoClick() {
							selfDialog.dismiss();
						}
					});
			selfDialog.show();

		} else {
			// 清理工作，略去
			// cheanUpdateFile(),文章后面我会附上代码
		}
	}

	/**
	 * 公共方法更新软件
	 */
	@SuppressWarnings("unused")
	private void CommonUpdateApp() {
		UpdateManager manager = new UpdateManager(MainActivity4.this);
		// 检查软件更新
		manager.checkUpdate();
	}

	// 用来计算返回键的点击间隔时间 点击两次退出程序
	private long exitTime = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			if ((System.currentTimeMillis() - exitTime) > 2000) {
				// 弹出提示，可以有多种方式
				Toast.makeText(getApplicationContext(), "再按一次退出程序",
						Toast.LENGTH_SHORT).show();
				exitTime = System.currentTimeMillis();
			} else {
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	// ---------------------------分享初始初始化和调用----------------------------------
	@SuppressWarnings("unused")
	private void showShare2() {
		OnekeyShare oks = new OnekeyShare();
		// 关闭sso授权
		oks.disableSSOWhenAuthorize();
		// 分享时Notification的图标和文字 2.5.9以后的版本不调用此方法
		// oks.setNotification(R.drawable.ic_launcher,
		// getString(R.string.app_name));
		// title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		oks.setTitle(getString(R.string.hello_world));
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl("http://sharesdk.cn");
		// text是分享文本，所有平台都需要这个字段
		oks.setText("我是分享文本");
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		// oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl("http://sharesdk.cn");
		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment("我是测试评论文本");
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite(getString(R.string.app_name));
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
		oks.setSiteUrl("http://sharesdk.cn");

		// 启动分享GUI
		oks.show(this);

	}

	private void showShare() {
		OnekeyShare oks = new OnekeyShare();
		// 关闭sso授权
		oks.disableSSOWhenAuthorize();
		// title标题，微信、QQ和QQ空间等平台使用
		oks.setTitle(getString(R.string.hello_world));
		// titleUrl QQ和QQ空间跳转链接
		oks.setTitleUrl("https://blog.csdn.net/dl10210950/article/details/52090973");
		// text是分享文本，所有平台都需要这个字段
		oks.setText("我是分享文本");
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		//oks.setImagePath("/sdcard/test.jpg");// 确保SDcard下面存在此张图片
		//oks.setImagePath("http://jintime.club:8080/images/upload/PeolePic/123.jpg");
		// url在微信、微博，Facebook等平台中使用
		oks.setUrl("http://sharesdk.cn");
		// comment是我对这条分享的评论，仅在人人网使用
		//oks.setComment("我是测试评论文本");
		// 启动分享GUI
		oks.show(this);
	}

}
