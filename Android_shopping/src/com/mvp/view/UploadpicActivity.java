package com.mvp.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONObject;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.shopping.R;
import com.mvp.model.Quanju;
import com.mvp.unitl.Base64Coder;
import com.mvp.unitl.HttpGetPostRequest;
import com.mvp.unitl.ImageUtils;
import com.mvp.unitl.common.CommonMath;

public class UploadpicActivity extends Activity {

	// --------定义全局变量------
	Quanju q = new Quanju(); // 定义全局类
	protected static final int CHOOSE_PICTURE = 0;
	protected static final int TAKE_PICTURE = 1;
	private static final int CROP_SMALL_PICTURE = 2;
	protected static Uri tempUri;
	private ImageView iv_personal_icon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_uploadpic);
		Button btn_change = (Button) findViewById(R.id.btn_change);
		iv_personal_icon = (ImageView) findViewById(R.id.iv_personal_icon);
		btn_change.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showChoosePicDialog();
			}
		});
	}

	/**
	 * 显示修改头像的对话框
	 */
	protected void showChoosePicDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("设置头像");
		String[] items = { "选择本地照片", "拍照" };
		builder.setNegativeButton("取消", null);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case CHOOSE_PICTURE: // 选择本地照片
					Intent openAlbumIntent = new Intent(
							Intent.ACTION_GET_CONTENT);
					openAlbumIntent.setType("image/*");
					startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
					break;
				case TAKE_PICTURE: // 拍照
					Intent openCameraIntent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					tempUri = Uri.fromFile(new File(Environment
							.getExternalStorageDirectory(), "image.jpg"));
					// 指定照片保存路径（SD卡），image.jpg为一个临时文件，每次拍照后这个图片都会被替换
					openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
					startActivityForResult(openCameraIntent, TAKE_PICTURE);
					break;
				}
			}
		});
		builder.create().show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) { // 如果返回码是可以用的
			switch (requestCode) {
			case TAKE_PICTURE:
				startPhotoZoom(tempUri); // 开始对图片进行裁剪处理
				break;
			case CHOOSE_PICTURE:
				startPhotoZoom(data.getData()); // 开始对图片进行裁剪处理
				break;
			case CROP_SMALL_PICTURE:
				if (data != null) {
					setImageToView(data); // 让刚才选择裁剪得到的图片显示在界面上
				}
				break;
			}
		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	protected void startPhotoZoom(Uri uri) {
		if (uri == null) {
			Log.i("tag", "The uri is not exist.");
		}
		tempUri = uri;
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 150);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, CROP_SMALL_PICTURE);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param
	 * @param picdata
	 */
	protected void setImageToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap photo = extras.getParcelable("data");
			// photo = BitmapUtil.toRoundBitmap(photo, tempUri);
			photo = ImageUtils.toRoundBitmap(photo);// 这个时候的图片已经被处理成圆形的了
			iv_personal_icon.setImageBitmap(photo);
			uploadPic(photo);
		}
	}

	/**
	 * 保存到本地再上传图片到服务器
	 * 
	 * @param bitmap
	 */
	@SuppressLint("ShowToast")
	private void uploadPic(Bitmap bitmap) {
		// 上传至服务器
		// ... 可以在这里把Bitmap转换成file，然后得到file的url，做文件上传操作
		// 注意这里得到的图片已经是圆形图片了
		// bitmap是没有做个圆形处理的，但已经被裁剪了
		try {
			/**
			 * 下面注释的方法是将裁剪之后的图片以Base64Coder的字符方式上 传到服务器，QQ头像上传采用的方法跟这个类似
			 */
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
			byte[] bytes = stream.toByteArray();
			// 将图片流以字符串形式存储下来
			final String picStr = new String(Base64Coder.encodeLines(bytes));
			new Thread(new Runnable() {
				@Override
				public void run() {
					// x-www-form-urlencoded 传参方式
					//List<NameValuePair> params = new ArrayList<NameValuePair>();
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("picStr", picStr);
					jsonObject.put("picName", String.valueOf(System.currentTimeMillis()));
					// params.add(new BasicNameValuePair("picStr", picStr));
					// params.add(new BasicNameValuePair("picName", "123"));
					// String.valueOf(System.currentTimeMillis())));
					// final String result = HttpGetPostRequest.httpPost(
					// q.postPicUrl, params);
					final String result = HttpGetPostRequest.httpJosnPost(
							q.postPicUrl, jsonObject.toString());
					runOnUiThread(new Runnable() {
						public void run() {
							Toast toast = Toast.makeText(
									UploadpicActivity.this, result,
									Toast.LENGTH_SHORT);
							new CommonMath().showMyToast(toast, 3 * 1000);

						}
					});
				}
			}).start();
			// String imagePath = ImageUtils.savePhoto(bitmap, Environment
			// .getExternalStorageDirectory().getAbsolutePath(), String
			// .valueOf(System.currentTimeMillis()));//currentTimeMillis 系统时间做名字
			// Log.e("imagePath", imagePath + "");
			// if (imagePath != null) {
			// // 拿着imagePath上传了
			// // ...
			// }
		} catch (Exception e) {
		}
	}
}