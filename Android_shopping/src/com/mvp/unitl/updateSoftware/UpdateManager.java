package com.mvp.unitl.updateSoftware;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RemoteViews;

import com.example.shopping.R;
import com.mvp.model.Quanju;
import com.mvp.unitl.ParseXmlService;
import com.mvp.unitl.SelfDialog;
import com.mvp.view.MainActivity4;

/**
 * 下载有进度条---在用--线程方式下载不佳
 * 
 * @author coolszy
 * @date 2012-4-26
 * @blog http://blog.92coding.com
 */

public class UpdateManager extends Activity {

	// 自定义变量
	private NotificationManager notificationManager;
	private Notification notification;

	private Intent updateIntent;
	private PendingIntent pendingIntent;
	private int notification_id = 0;
	long totalSize = 0;// 文件总大小
	private static final int DOWN_ERROR = 0;

	// -------定义全局类----------
	Quanju q = new Quanju();// 获取所有表数据;
	/* 下载中 */
	private static final int DOWNLOAD = 1;
	/* 下载结束 */
	private static final int DOWNLOAD_FINISH = 2;
	/* 保存解析的XML信息 */
	HashMap<String, String> mHashMap = new HashMap<String, String>();
	/* 下载保存路径 */
	private String mSavePath;
	/* 记录进度条数量 */
	private int progress;
	// 当前进度条进度
	private double tempresult;
	/* 是否取消更新 */
	private boolean cancelUpdate = false;

	private Context mContext = this;
	/* 更新进度条 */
	private ProgressBar mProgress;
	// 下载弹窗
	private Dialog mDownloadDialog;

	/**
	 * ---原理是用线程方式 会让下载完成才可操作 不可取
	 */
	private Handler mHandler1 = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			// 正在下载
			case DOWNLOAD:
				// 设置进度条位置
				mProgress.setProgress(progress);
				break;
			case DOWNLOAD_FINISH:
				// 安装文件
				installApk();
				break;
			default:
				break;
			}
		};
	};

	/***
	 * 更新UI 此消息通知下载app不好---原理是用线程方式 会让下载完成才可操作 不可取
	 */
	private Handler mHandler = new Handler() {
		@Override
		@SuppressWarnings("deprecation")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			// 正在下载
			case DOWNLOAD:
				// 设置进度条位置
				// mProgress.setProgress(progress);

				DecimalFormat df1 = new DecimalFormat("0.00"); // ##.00%
				// 百分比格式，后面不足2位的用0补齐
				String result = df1.format(tempresult);
				contentView.setTextViewText(R.update_id.tvProcess,
						(int) (Float.parseFloat(result) * 100) + "%");
				contentView.setProgressBar(R.update_id.pbDownload, 100,
						(int) (Float.parseFloat(result) * 100), false);
				// contentView.setTextViewText(R.update_id.tvProcess, progress
				// + "%");
				// contentView.setProgressBar(R.update_id.pbDownload, 100,
				// progress, false);
				// 有问题--跳转出问题
				notificationManager.notify(notification_id, notification);
				break;
			case DOWNLOAD_FINISH:
				notificationManager.cancelAll();
				// // 下载完成，点击安装
				Intent installApkIntent = getFileIntent(new File(mSavePath,
						mHashMap.get("name")));
				pendingIntent = PendingIntent.getActivity(mContext, 0,
						installApkIntent, 0);
				notification.contentIntent = pendingIntent;
				notification.flags |= Notification.FLAG_AUTO_CANCEL;
				// notification.setLatestEventInfo(mContext, q.appName,
				// "下载成功，点击安装", pendingIntent);
				contentView.setTextViewText(R.update_id.tvProcess, "");
				contentView.setTextViewText(R.update_id.notificationTitle,
						"下载成功，点击安装");
				notificationManager.notify(notification_id, notification);
				// // 安装文件
				// installApk();
				mContext.startActivity(installApkIntent);
				break;
			case DOWN_ERROR:
				contentView.setTextViewText(R.update_id.tvProcess, "");
				contentView.setTextViewText(R.update_id.notificationTitle,
						"下载失败");
				notificationManager.notify(notification_id, notification);
				break;
			default:
				notificationManager.cancelAll();
				break;
			}
		}
	};

	public UpdateManager(Context context) {
		this.mContext = context;
	}

	/**
	 * 检测软件更新
	 */
	public void checkUpdate() {
		StringBuffer string = new StringBuffer();
		if (isUpdate(string)) {
			// 显示提示对话框
			showNoticeDialog(string);
		} else {
			// Toast.makeText(mContext, R.string.soft_update_no,
			// Toast.LENGTH_LONG)
			// .show();
		}
	}

	/**
	 * 检查软件是否有更新版本
	 * 
	 * @return
	 */
	private boolean isUpdate(final StringBuffer content) {
		// 获取当前软件版本
		int versionCode = getVersionCode(mContext);
		// 把version.xml放到网络上，然后获取文件信息
		InputStream inStream = ParseXmlService.class.getClassLoader()
				.getResourceAsStream("version.xml");
		// 解析XML文件。 由于XML文件比较小，因此使用DOM方式进行解析
		ParseXmlService service = new ParseXmlService();
		try {
			// new Thread() {
			// public void run() {
			// String url = q.getVersionUrl;
			// String param = "name=" + q.appName;
			// ResponseData r = HttpGetPostRequest
			// .GetData2(HttpGetPostRequest.sendGet(url, param));
			// if (r != null && r.Code == 1) // 成功操作
			// {
			// Version version = HttpGetPostRequest
			// .paraseVersion(r.Models.toString());
			// if (version.id > 0) {
			// content.append(version.content);
			// mHashMap.put("code", "ok");
			// mHashMap.put("name", version.name);
			// mHashMap.put("url", version.url);
			// mHashMap.put("version", version.version + "");
			// } else {
			// mHashMap.put("code", "no");
			// }
			// }
			// }
			// }.start();
			// 本地测试
			mHashMap = service.parseXml(inStream);
			content.append(mHashMap.get("content"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 主线程延迟一秒 获取链接先在做下面操作
		SystemClock.sleep(1000);
		// do {
		if (mHashMap.size() > 0 && mHashMap.get("code").equals("ok")) {
			int serviceCode = Integer.valueOf(mHashMap.get("version"));
			// 版本判断
			if (serviceCode > versionCode) {
				return true;
			}
		}
		// }while (mHashMap.size()==0);
		return false;
	}

	/**
	 * 获取软件版本号
	 * 
	 * @param context
	 * @return
	 */
	private int getVersionCode(Context context) {
		int versionCode = 0;
		try {
			// 获取软件版本号，对应AndroidManifest.xml下android:versionCode
			versionCode = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionCode;
	}

	/**
	 * 显示软件更新对话框
	 */
	// 自定义弹窗
	SelfDialog selfDialog;

	@SuppressLint("NewApi")
	private void showNoticeDialog(StringBuffer Message) {
		// 构造对话框
		selfDialog = new SelfDialog(mContext);
		selfDialog.setTitle("软件更新");
		if (!Message.toString().isEmpty()) {
			Message.insert(0, "检测到新版本,立即更新吗\n");// ="检测到新版本,立即更新吗\n"+Message;
		} else {
			Message.append("检测到新版本,立即更新吗");
		}
		selfDialog.setMessage(Message.toString().replace("\\n", "\n"));
		selfDialog.setYesOnclickListener("立即更新",
				new SelfDialog.onYesOnclickListener() {
					@Override
					public void onYesClick() {
						selfDialog.dismiss();
						// 显示下载对话框 弹出窗形式
						showDownloadDialog();
						// 通知栏形式
						// showDownloadDialog2();
					}
				});
		selfDialog.setNoOnclickListener("暂不更新",
				new SelfDialog.onNoOnclickListener() {
					@Override
					public void onNoClick() {
						selfDialog.dismiss();
					}
				});
		selfDialog.show();
	}

	/**
	 * 显示软件下载对话框
	 */
	RemoteViews contentView;

	@SuppressWarnings("deprecation")
	private void showDownloadDialog2() {
		try {
			notificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notification = new Notification();
			notification.icon = R.drawable.logo2;
			// notification.icon = R.drawable.ic_launcher;
			// 这个参数是通知提示闪出来的值.
			notification.tickerText = "开始下载";
			// pendingIntent = PendingIntent.getActivity(mContext, 0,
			// updateIntent, 0);

			// 这里面的参数是通知栏view显示的内容
			// notification.setLatestEventInfo(mContext, q.appName, "下载：0%",
			// pendingIntent);

			// notificationManager.notify(notification_id, notification);

			/***
			 * 在这里我们用自定的view来显示Notification
			 */
			contentView = new RemoteViews(mContext.getPackageName(),
					R.layout.down_hint);
			contentView.setTextViewText(R.update_id.notificationTitle, "正在下载");
			contentView.setTextViewText(R.update_id.tvProcess, "0%");
			contentView.setProgressBar(R.update_id.pbDownload, 100, 0, false);
			// 自定义的进度条
			// mProgress = (ProgressBar) findViewById(R.update_id.pbDownload);
			notification.contentView = contentView;

			updateIntent = new Intent(mContext, MainActivity4.class);
			updateIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			pendingIntent = PendingIntent.getActivity(mContext, 0,
					updateIntent, 0);

			notification.contentIntent = pendingIntent;
			notificationManager.notify(notification_id, notification);
			// 下载文件
			downloadApk2();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 显示软件下载对话框
	 */
	private void showDownloadDialog() {
		// 构造软件下载对话框
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(R.string.soft_updating);
		// 给下载对话框增加进度条
		final LayoutInflater inflater = LayoutInflater.from(mContext);
		View v = inflater.inflate(R.layout.softupdate_progress, null);
		mProgress = (ProgressBar) v.findViewById(R.id.update_progress);
		builder.setView(v);
		// 取消更新
		builder.setNegativeButton(R.string.soft_update_cancel,
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 设置取消状态
						cancelUpdate = true;
					}
				});
		mDownloadDialog = builder.create();
		mDownloadDialog.show();
		// 下载文件
		downloadApk();
	}

	/**
	 * 下载apk文件
	 */
	private void downloadApk2() {
		// 启动新线程下载软件
		new downloadApkThread2().start();
	}

	/**
	 * 下载文件线程
	 * 
	 * @author coolszy
	 * @date 2012-4-26
	 * @blog http://blog.92coding.com
	 */
	private class downloadApkThread2 extends Thread {
		@Override
		public void run() {
			try {
				// 判断SD卡是否存在，并且是否具有读写权限
				if (Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					// 获得存储卡的路径
					String sdpath = Environment.getExternalStorageDirectory()
							+ "/";
					mSavePath = sdpath + "download";
					URL url = new URL(mHashMap.get("url"));
					// 创建连接
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.connect();
					// 获取文件大小
					int length = conn.getContentLength();
					// 创建输入流
					InputStream is = conn.getInputStream();

					File file = new File(mSavePath);
					// 判断文件目录是否存在
					if (!file.exists()) {
						file.mkdir();
					}
					File apkFile = new File(mSavePath, mHashMap.get("name"));
					FileOutputStream fos = new FileOutputStream(apkFile);
					int count = 0;
					// 缓存
					byte buf[] = new byte[1024];
					// 写入到文件中
					do {
						int numread = is.read(buf);
						count += numread;
						double x_double = count * 1.0;
						tempresult = x_double / length;
						mHandler.sendEmptyMessage(DOWNLOAD);
						if (numread <= 0) {
							// 下载完成
							mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
							break;
						}
						// 写入文件
						fos.write(buf, 0, numread);
					} while (!cancelUpdate);// 点击取消就停止下载.
					fos.close();
					is.close();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * 下载apk文件
	 */
	private void downloadApk() {
		// 启动新线程下载软件
		new downloadApkThread().start();
	}

	/**
	 * 下载文件线程
	 * 
	 * @author coolszy
	 * @date 2012-4-26
	 * @blog http://blog.92coding.com
	 */
	private class downloadApkThread extends Thread {
		@Override
		public void run() {
			try {
				// 判断SD卡是否存在，并且是否具有读写权限
				if (Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					// 获得存储卡的路径
					String sdpath = Environment.getExternalStorageDirectory()
							+ "/";
					mSavePath = sdpath + "download";
					URL url = new URL(mHashMap.get("url"));
					// 创建连接
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.connect();
					// 获取文件大小
					int length = conn.getContentLength();
					// 创建输入流
					InputStream is = conn.getInputStream();

					File file = new File(mSavePath);
					// 判断文件目录是否存在
					if (!file.exists()) {
						file.mkdir();
					}
					File apkFile = new File(mSavePath, mHashMap.get("name"));
					FileOutputStream fos = new FileOutputStream(apkFile);
					int count = 0;
					// 缓存
					byte buf[] = new byte[1024];
					// 写入到文件中
					do {
						int numread = is.read(buf);
						count += numread;
						// 计算进度条位置
						progress = (int) (((float) count / length) * 100);
						// 更新进度
						mHandler.sendEmptyMessage(DOWNLOAD);
						if (numread <= 0) {
							// 下载完成
							mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
							break;
						}
						// 写入文件
						fos.write(buf, 0, numread);
					} while (!cancelUpdate);// 点击取消就停止下载.
					fos.close();
					is.close();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// 取消下载对话框显示
			mDownloadDialog.dismiss();
		}
	};

	/**
	 * 安装APK文件
	 */
	private void installApk() {
		File apkfile = new File(mSavePath, mHashMap.get("name"));
		if (!apkfile.exists()) {
			return;
		}
		// 通过Intent安装APK文件
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
				"application/vnd.android.package-archive");
		mContext.startActivity(i);
	}

	public static Intent getFileIntent(File file) {
		// 通过Intent安装APK文件
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + file.toString()),
				"application/vnd.android.package-archive");
		// 查看文件可以用
		// Uri uri = Uri.fromFile(file);
		// String type = getMIMEType(file);
		// Intent intent = new Intent("android.intent.action.VIEW");
		// intent.addCategory("android.intent.category.DEFAULT");
		// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.setDataAndType(uri, type);
		return intent;
	}

	public static String getMIMEType(File f) {
		String type = "";
		String fName = f.getName();
		// 取得扩展名
		String end = fName
				.substring(fName.lastIndexOf(".") + 1, fName.length());
		if (end.equals("apk")) {
			type = "application/vnd.android.package-archive";
		} else {
			// /*如果无法直接打开，就跳出软件列表给用户选择 */
			type = "*/*";
		}
		return type;
	}

}
