package com.mvp.unitl.updateSoftware;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;

import com.example.shopping.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.mvp.view.MainActivity;

/**
 * 通过在AndroidManifest.xml 配置service 服务下载  与线程下载区别不影响正常操作--比较优势  
 * @author admin
 *
 */
public class UpdateService extends Service {
	private static String down_url; // = "http://192.168.1.112:8080/360.apk";
	private static final int DOWN_OK = 1; // 下载完成
	private static final int DOWN_ERROR = 0;
	/* 下载保存路径 */
	private String mSavePath;
	// 保存的app名称
	private String app_name;
	private Context mContext;
	private NotificationManager notificationManager;
	private Notification notification;

	private Intent updateIntent;
	private PendingIntent pendingIntent;
	// 文件全路径
	private String updateFile;

	private int notification_id = 0;
	long totalSize = 0;// 文件总大小
	/***
	 * 更新UI
	 */
	final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWN_OK:
				notificationManager.cancelAll();
				// 下载完成，点击安装
				Intent installApkIntent = getFileIntent(new File(updateFile));
				pendingIntent = PendingIntent.getActivity(mContext, 0,
						installApkIntent, 0);
				notification.contentIntent = pendingIntent;
				notification.flags |= Notification.FLAG_AUTO_CANCEL;
				// notification.setLatestEventInfo(mContext, q.appName,
				// "下载成功，点击安装", pendingIntent);
				contentView.setTextViewText(R.update_id.tvProcess, "");
				contentView.setTextViewText(R.update_id.notificationTitle,
						"下载成功，点击安装");
				notificationManager.notify(notification_id, notification);
				//startActivity(installApkIntent);
				stopService(updateIntent);
				break;
			case DOWN_ERROR:
				contentView.setTextViewText(R.update_id.tvProcess, "");
				contentView.setTextViewText(R.update_id.notificationTitle,
						"下载失败");
				notificationManager.notify(notification_id, notification);
				break;
			default:
				notificationManager.cancelAll();
				stopService(updateIntent);
				break;
			}
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			try {
				app_name = intent.getStringExtra("app_name");
				down_url = intent.getStringExtra("downurl");
				// 判断SD卡是否存在，并且是否具有读写权限
				if (Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					// 获得存储卡的路径
					String sdpath = Environment.getExternalStorageDirectory()
							+ "/";
					mSavePath = sdpath + "download";
					// 创建文件
					File updateFile = new File(mSavePath, app_name);

					if (!updateFile.exists()) {
						try {
							updateFile.createNewFile();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					// 创建通知
					createNotification();
					// 开始下载
					downloadUpdateFile(down_url, updateFile.getAbsolutePath());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return super.onStartCommand(intent, flags, startId);
	}

	/***
	 * 创建通知栏
	 */
	RemoteViews contentView;

	@SuppressWarnings("deprecation")
	public void createNotification() {

		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notification = new Notification();
		notification.icon = R.drawable.logo2;
		// notification.icon = R.drawable.ic_launcher;
		// 这个参数是通知提示闪出来的值.
		notification.tickerText = "开始下载";

		pendingIntent = PendingIntent.getActivity(this, 0, updateIntent, 0);

		// 这里面的参数是通知栏view显示的内容
		// notification.setLatestEventInfo(this, app_name, "下载：0%",
		// pendingIntent);

		notificationManager.notify(notification_id, notification);

		/***
		 * 在这里我们用自定的view来显示Notification
		 */
		contentView = new RemoteViews(getPackageName(), R.layout.down_hint);
		contentView.setTextViewText(R.update_id.notificationTitle, "正在下载");
		contentView.setTextViewText(R.update_id.tvProcess, "0%");
		contentView.setProgressBar(R.update_id.pbDownload, 100, 0, false);

		notification.contentView = contentView;

		updateIntent = new Intent(this, MainActivity.class);
		updateIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		pendingIntent = PendingIntent.getActivity(this, 0, updateIntent, 0);

		notification.contentIntent = pendingIntent;
		notificationManager.notify(notification_id, notification);
	}

	/***
	 * 下载apk文件
	 */
	public void downloadUpdateFile(String down_url, String file) {
		updateFile = file;
		HttpUtils HttpUtils = new HttpUtils();
		try {
			HttpUtils.download(down_url, file, new RequestCallBack<File>() {
				/**
				 * 下载成功
				 */
				@Override
				public void onSuccess(ResponseInfo<File> responseInfo) {
					// 下载成功
					Message message = handler.obtainMessage();
					message.what = DOWN_OK;
					handler.sendMessage(message);
					notificationManager.cancelAll();
					installApk(new File(updateFile), UpdateService.this);
				}

				/**
				 * 下载失败
				 */
				@Override
				public void onFailure(HttpException error, String msg) {
					Message message = handler.obtainMessage();
					message.what = DOWN_ERROR;
					handler.sendMessage(message);
				}

				/**
				 * 下载中
				 */
				@Override
				public void onLoading(long total, long current,
						boolean isUploading) {
					try {
						super.onLoading(total, current, isUploading);
						double x_double = current * 1.0;
						double tempresult = x_double / total;
						DecimalFormat df1 = new DecimalFormat("0.00"); // ##.00%
						// 百分比格式，后面不足2位的用0补齐
						String result = df1.format(tempresult);
						contentView.setTextViewText(R.update_id.tvProcess,
								(int) (Float.parseFloat(result) * 100) + "%");
						contentView.setProgressBar(R.update_id.pbDownload, 100,
								(int) (Float.parseFloat(result) * 100), false);
						// 有问题--跳转出问题
						notificationManager.notify(notification_id,
								notification);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 下载完成后打开安装apk界面
	public static void installApk(File apkfile, Context mContext) {
		try {
			// File apkfile = new File(mSavePath,"appname" );
			if (!apkfile.exists()) {
				return;
			}
			// 通过Intent安装APK文件
			// L.i("msg", "版本更新获取sd卡的安装包的路径=" + file.getAbsolutePath());
			Intent openFile = getFileIntent(apkfile);
			mContext.startActivity(openFile);
		} catch (Exception e) {
		}
	}

	public static Intent getFileIntent(File file) {
		// 通过Intent安装APK文件
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + file.toString()),
				"application/vnd.android.package-archive");
		// Uri uri = Uri.fromFile(file);
		// String type = getMIMEType(file);
		// Intent intent = new Intent("android.intent.action.VIEW");
		// intent.addCategory("android.intent.category.DEFAULT");
		// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.setDataAndType(uri, type);
		return intent;
	}

	public static String getMIMEType(File f) {
		String type = "";
		String fName = f.getName();
		// 取得扩展名
		String end = fName
				.substring(fName.lastIndexOf(".") + 1, fName.length());
		if (end.equals("apk")) {
			type = "application/vnd.android.package-archive";
		} else {
			// /*如果无法直接打开，就跳出软件列表给用户选择 */
			type = "*/*";
		}
		return type;
	}
}
