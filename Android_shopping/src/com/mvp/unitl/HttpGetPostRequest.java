package com.mvp.unitl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mvp.model.Version;
import com.mvp.model.response.ResponseData;

/**
 * getpost帮助类 在用
 * 
 * @author admin
 */
@SuppressLint("NewApi")
public class HttpGetPostRequest {
	/**
	 * 向指定URL发送GET方法的请求
	 * 
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));// 防止乱码
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 返回数据状态及其数据(java)
	 * 
	 * @param datastr
	 * @return
	 */
	public static ResponseData GetData(String datastr) {
		ResponseData responseData = null;
		if (datastr != "") {
			responseData = new ResponseData();
			JSONObject jsonObject = JSONObject.fromObject(datastr);
			responseData.setMsg(jsonObject.getString("msg"));
			responseData.setCode(jsonObject.getInt("code"));
			responseData.setData(jsonObject.getString("data"));
		}
		return responseData;
	}

	/**
	 * webAPI接口返回数据状态及其数据(c#)
	 * 
	 * @param datastr
	 * @return
	 */
	public static ResponseData GetData2(String datastr) {
		ResponseData responseData = null;
		if (datastr != "") {
			responseData = new ResponseData();
			JSONObject jsonObject = JSONObject.fromObject(datastr);
			responseData.Message = jsonObject.getString("Message");
			responseData.Code = jsonObject.getInt("Code");
			responseData.Models = jsonObject.getString("Models");
		}
		return responseData;
	}

	/**
	 * 获取版本数据
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Version paraseVersion(String datapart) {
		Gson gson = new Gson();
		// 这两句代码必须的，为的是初始化出来gson这个对象，才能拿来用
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		Version resourcebeans = new Version();
		if (datapart != "") {
			try {
				/*
				 * datapart="["+datapart+"]"; JSONArray objData =
				 * JSONArray.fromObject(datapart); //获取data字段
				 * resourcebeans=(Version)JSONArray.toCollection(objData,
				 * Version.class);
				 */
				JSONObject jsonObject = JSONObject.fromObject(datapart);
				java.lang.reflect.Type type0 = new TypeToken<Version>() {
				}.getType();
				String str = jsonObject.getString("Version"); // jsonObject.toString();
				resourcebeans = gson.fromJson(str, type0);
			} catch (Exception e) {
				System.out.println("请求出现异常！" + e);
				e.printStackTrace();
			}
		}
		return resourcebeans;
	}
	
	/**
	 * post提交数据x-www-form-urlencoded 传参方式“&”符号之后一连串都是参数
	 * @param url
	 * @param params
	 * @return
	 */
	public static String httpPost(String url,List<NameValuePair> params){
		String result = "";
		HttpPost httpRequest = new HttpPost(url);
		try {
			httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
			httpRequest.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
			result = EntityUtils.toString(httpResponse.getEntity());		
		} catch (UnsupportedEncodingException e) {
			result = "URL有误";
		} catch (ClientProtocolException e) {
			result = "连接失败";
		} catch (IOException e) {
			result = "服务器有误";
		}
		return result;
	}
	
	/**
	 * post提交数据Josn 传参方式
	 * @param url
	 * @param params
	 * @return
	 */
	public static String httpJosnPost(String url,String body){
		String result = "";
		HttpPost httpRequest = new HttpPost(url);
		try {
			httpRequest.addHeader("Content-Type", "application/json");
			httpRequest.setEntity(new StringEntity(body));
			HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
			result = EntityUtils.toString(httpResponse.getEntity());		
		} catch (UnsupportedEncodingException e) {
			result = "URL有误";
		} catch (ClientProtocolException e) {
			result = "连接失败";
		} catch (IOException e) {
			result = "服务器有误";
		}
		return result;
	}
	


}
