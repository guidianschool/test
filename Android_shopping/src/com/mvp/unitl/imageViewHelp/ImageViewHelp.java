package com.mvp.unitl.imageViewHelp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.example.shopping.R;
import com.mvp.unitl.imageViewHelp.ImageAsyncLoader.ImageCallback;

/**
 * 图片异步加载相对Handler 速度快 好用
 * 
 * @author admin
 *
 */
public class ImageViewHelp {
	/**
	 * 加载图片image.setImageDrawable相对于image.setImageBitmap速度快
	 * 
	 * @param context
	 *            当前activity 上下文环境
	 * @param image
	 *            imageview控件
	 * @param imageUrl
	 *            加载网络图片路径
	 * @param w
	 *            图片宽度
	 * @param h
	 *            图片高度
	 */
	public static void loadImage(final Context context, final ImageView image,
			final String imageUrl, final int w, final int h) {
		// 自定义大小 例子之前150x150
		// 公共方法
		thisCommon(context, image, imageUrl, w, h);

	}

	/**
	 * loadImage 方法重构
	 * 
	 * @param context
	 *            当前activity 上下文环境
	 * @param image
	 *            imageview控件
	 * @param imageUrl
	 *            加载网络图片路径
	 * @param w
	 *            图片宽度
	 * @param h
	 *            图片高度
	 */
	public static void loadImage(final Context context, final ImageView image,
			final String imageUrl) {
		// 加载原图大小
		// 公共方法
		thisCommon(context, image, imageUrl);

	}

	/**
	 * 提取公共方法
	 * 
	 * @param context
	 * @param image
	 * @param imageUrl
	 * @param w
	 * @param h
	 */
	private static void thisCommon(final Context context,
			final ImageView image, final String imageUrl, final int w,
			final int h) {
		ImageAsyncLoader asyncImageLoader = new ImageAsyncLoader();
		// 异步加载图片
		Drawable cachedImage = asyncImageLoader.loadDrawable(context, imageUrl,
				new ImageCallback() {
					@Override
					public void imageLoaded(Drawable imageDrawable,
							String imageUrl) {
						if (imageDrawable != null) {
							image.setImageDrawable(ImageAsyncLoader
									.zoomDrawable(
											imageDrawable,
											ImageAsyncLoader.dip2px(context, w),
											ImageAsyncLoader.dip2px(context, h)));
						}
					}
				});
		if (cachedImage != null) {
			image.setImageDrawable(ImageAsyncLoader.zoomDrawable(cachedImage,
					ImageAsyncLoader.dip2px(context, w),
					ImageAsyncLoader.dip2px(context, h)));
		}
	}

	private static void thisCommon(final Context context,
			final ImageView image, final String imageUrl) {
		ImageAsyncLoader asyncImageLoader = new ImageAsyncLoader();
		// 异步加载图片
		Drawable cachedImage = asyncImageLoader.loadDrawable(context, imageUrl,
				new ImageCallback() {
					@Override
					public void imageLoaded(Drawable imageDrawable,
							String imageUrl) {
						if (imageDrawable != null) {
							image.setImageDrawable(imageDrawable);
						}else	
							image.setImageResource(R.drawable.r404);
					}
				});
		if (cachedImage != null) {
			image.setImageDrawable(cachedImage);
		}else	
			image.setImageResource(R.drawable.r404);
	}

}
