package com.mvp.unitl;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.shopping.R;

/**
 * 创建自定义的dialog(弹窗)，主要学习其实现原理 模式:标题+内容+是+否 Created by chengguo on 2016/3/22.
 */
public class SelfDialog extends Dialog {

	private Button yes;// 确定按钮
	private View yesnoview;// 按钮间隔线
	private Button no;// 取消按钮
	private TextView titleTv;// 消息标题文本
	private TextView messageTv;// 消息提示文本
	private String titleStr;// 从外界设置的title文本
	private String messageStr;// 从外界设置的消息文本
	// private View viewid;// 是否加载进度条
	// private View viewValue;// 是否加载进度条的值
	// 确定文本和取消文本的显示内容
	private String yesStr, noStr, noStr2;

	private onNoOnclickListener noOnclickListener;// 取消按钮被点击了的监听器
	private onYesOnclickListener yesOnclickListener;// 确定按钮被点击了的监听器

	/**
	 * 设置取消按钮的显示内容和监听
	 * @param str
	 * @param onNoOnclickListener
	 */
	public void setNoOnclickListener(String str,
			onNoOnclickListener onNoOnclickListener) {
		if (str != null) {
			noStr = str;
		}
		this.noOnclickListener = onNoOnclickListener;
	}

	/**
	 * 设置确定按钮的显示内容和监听
	 *
	 * @param str
	 * @param onYesOnclickListener
	 */
	public void setYesOnclickListener(String str,
			onYesOnclickListener onYesOnclickListener) {
		if (str != null) {
			yesStr = str;
		}
		this.yesOnclickListener = onYesOnclickListener;
	}

	public SelfDialog(Context context) {
		super(context, R.style.MyDialog);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.confirmdialog);
		// 按空白处不能取消动画
		setCanceledOnTouchOutside(false);

		// 初始化界面控件
		initView();
		// 初始化界面数据
		initData();
		// 初始化界面控件的事件
		initEvent();

	}

	/**
	 * 初始化界面的确定和取消监听器
	 */
	private void initEvent() {
		// 设置确定按钮被点击后，向外界提供监听
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (yesOnclickListener != null) {
					yesOnclickListener.onYesClick();
				}
			}
		});
		// 设置取消按钮被点击后，向外界提供监听
		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (noOnclickListener != null) {
					noOnclickListener.onNoClick();
				}
			}
		});
	}

	/**
	 * 初始化界面控件的显示数据
	 */
	private void initData() {
		// 如果用户自定了title和message
		if (titleStr != null) {
			titleTv.setText(titleStr);
		}
		if (messageStr != null) {
			messageTv.setText(messageStr);
			/*
			 * // 隐藏进度条 viewid.setVisibility(View.GONE); // 显示提示消息
			 * messageTv.setVisibility(View.VISIBLE);
			 */
		}
		int cout = 0;
		// 如果设置按钮的文字//根据传入的值显示对应的按钮
		if (yesStr != null) {
			yes.setText(yesStr);
			yes.setVisibility(View.VISIBLE);
			cout++;
		} else {
			yes.setVisibility(View.GONE);
		}
		if (noStr != null) {
			no.setText(noStr);
			no.setVisibility(View.VISIBLE);
			cout++;
		} else {
			no.setVisibility(View.GONE);
		}
		if (cout == 1) {
			yesnoview.setVisibility(View.GONE);
		} else {
			yesnoview.setVisibility(View.VISIBLE);
		}
		// 加载进度条
		// if (viewValue != null) {
		// viewid = viewValue;
		// // 显示
		// viewid.setVisibility(View.VISIBLE);
		// // 隐藏
		// messageTv.setVisibility(View.GONE);
		// }
	}

	/**
	 * 初始化界面控件
	 */
	private void initView() {
		yes = (Button) findViewById(R.id.yes);
		no = (Button) findViewById(R.id.no);
		titleTv = (TextView) findViewById(R.id.title);
		messageTv = (TextView) findViewById(R.id.message);
		yesnoview = (View) findViewById(R.id.yesnoview);
		// viewid = (View) findViewById(R.id.view);

	}

	/**
	 * 从外界Activity为Dialog设置标题
	 *
	 * @param title
	 */
	public void setTitle(String title) {
		titleStr = title;
	}

	/**
	 * 从外界Activity为Dialog设置dialog的message
	 *
	 * @param message
	 */
	public void setMessage(String message) {
		messageStr = message;
	}

	/**
	 * 视图值
	 */
	// public void setView(View view) {
	// viewValue = view;
	// }

	/**
	 * 设置确定按钮和取消被点击的接口
	 */
	public interface onYesOnclickListener {
		public void onYesClick();
	}

	public interface onNoOnclickListener {
		public void onNoClick();
	}
}
