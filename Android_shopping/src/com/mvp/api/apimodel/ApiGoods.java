package com.mvp.api.apimodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.mvp.unitl.common.CommonMath;

//商品表
public class ApiGoods {
	int id;
	int sort;
	String merchantName; // 商家名
	String name;// 商品名称
	double price;// 单价
	String uint;// 包装单位
	int classifyID;// 分类ID
	String classify;// 分类名
	String intro;// 简介
	String image;// 图片
	int vistNum = 0; // 访问量

	public ApiGoods() {
	}


	public ApiGoods(int id, int sort, String merchantName, String name,
			double price, String uint, int classifyID, String classify,
			String intro, String image, int vistNum) {
		super();
		this.id = id;
		this.sort = sort;
		this.merchantName = merchantName;
		this.name = name;
		this.price = price;
		this.uint = uint;
		this.classifyID = classifyID;
		this.classify = classify;
		this.intro = intro;
		this.image = image;
		this.vistNum = vistNum;
	}


	// ***************公共方法***************************
	// 增加忽略

	// 模糊(商品名称或类别名\商家名)查询列表
	public static List<ApiGoods> selectGoodsByNameOrClass(List<ApiGoods> list,
			String findStr) {
		List<ApiGoods> relist = new ArrayList<ApiGoods>();
		for (ApiGoods goods : list) {
			if (goods.name.contains(findStr)
					|| goods.classify.contains(findStr)
					|| goods.merchantName.contains(findStr)) {
				relist.add(goods); // 添加数据给list集合
			}
		}
		return relist;
	}

	// 返回列表投几条数据
	public static List<ApiGoods> selectGoodsByTop(List<ApiGoods> list, int findNum) {
		List<ApiGoods> relist = new ArrayList<ApiGoods>();
		if (findNum > list.size()) {
			findNum = list.size();
		}
		relist = list.subList(0, findNum);
		return relist;
	}

	// 数据组装对应模型
	public static ArrayList<HashMap<String, Object>> getListToHashMap(
			List<ApiGoods> list) {
		ArrayList<HashMap<String, Object>> list2 = new ArrayList<HashMap<String, Object>>();
		for (ApiGoods goods : list) {
			// 添加数据给map集合
			HashMap<String, Object> map1 = new HashMap<String, Object>(); // 定义map集合(数组），一个map集合对应ListView的一栏。
			map1.put("MerchantName", goods.merchantName);
			map1.put("Price", goods.price);
			map1.put("PriceStr", "¥ " + goods.price);
			map1.put("Name", goods.name);
			map1.put("UintName", goods.uint + "    " + goods.name);
			map1.put("Classify", goods.classify);
			map1.put("Intro", goods.intro);
			map1.put("GoodsID", goods.id);
			map1.put("Image", CommonMath.getImageResourceID(goods.image));
			// -----把map集合放进list集合里---------
			list2.add(map1); // 添加数据给list集合
		}
		return list2;
	}

	// 查询单个记录
	public static ApiGoods selectGoodsByID(List<ApiGoods> list, int id) {
		ApiGoods l = new ApiGoods();
		for (ApiGoods goods : list) {
			if (goods.id == id) {
				l = goods;
				break;
			}
		}
		return l;
	}

	// 排序通过属性排序asc升1 des降序0
	public static List<ApiGoods> sortGoodsListBySort(List<ApiGoods> list,final int ascOrdes ) {
		Comparator<ApiGoods> comparator = new Comparator<ApiGoods>() {
			@Override
			public int compare(ApiGoods s1, ApiGoods s2) {
				if (ascOrdes==1) {
					// 先排序号
					if (s1.sort != s2.sort) {
						return s1.sort - s2.sort;
					} else {
						// 年龄相同则按姓名排序
						if (!s1.name.equals(s2.name)) {
							return s1.name.compareTo(s2.name);
						} else {
							// 姓名也相同则按学号排序
							return s1.id - s2.id;
						}
					}
				}else {
					// 先排序号
					if (s1.sort != s2.sort) {
						return s2.sort - s1.sort;
					} else {
						// 年龄相同则按姓名排序
						if (!s1.name.equals(s2.name)) {
							return s1.name.compareTo(s2.name);
						} else {
							// 姓名也相同则按学号排序
							return s2.id - s1.id;
						}
					}
				}
			}
		};
		// 这里就会自动根据规则进行排序
		Collections.sort(list, comparator);
		return list;
	}

	// 删除一条记录
	public boolean deleteGoodsByID(List<ApiGoods> list, int id) {
		boolean b = false;
		ApiGoods l = new ApiGoods();
		for (ApiGoods goods : list) {
			if (goods.id == id) {
				l = goods;
				b = true;
				break;
			}
		}
		list.remove(l);
		return b;
	}

	// 修改一条记录通过id
	public boolean updateNameGoodsByID(List<ApiGoods> list, int id, String name) {
		boolean b = false;
		for (ApiGoods goods : list) {
			if (goods.id == id) {
				goods.name = name;
				b = true;
				break;
			}
		}
		return b;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSort() {
		return sort;
	}


	public void setSort(int sort) {
		this.sort = sort;
	}


	public String getMerchantName() {
		return merchantName;
	}


	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getUint() {
		return uint;
	}


	public void setUint(String uint) {
		this.uint = uint;
	}


	public int getClassifyID() {
		return classifyID;
	}


	public void setClassifyID(int classifyID) {
		this.classifyID = classifyID;
	}


	public String getClassify() {
		return classify;
	}


	public void setClassify(String classify) {
		this.classify = classify;
	}


	public String getIntro() {
		return intro;
	}


	public void setIntro(String intro) {
		this.intro = intro;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public int getVistNum() {
		return vistNum;
	}


	public void setVistNum(int vistNum) {
		this.vistNum = vistNum;
	}

}
