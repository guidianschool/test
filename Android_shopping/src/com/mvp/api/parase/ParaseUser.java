package com.mvp.api.parase;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mvp.model.User;

public class ParaseUser {

	/**
	 * 获取数据
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static User paraseUser(String datapart) {
		Gson gson = new Gson();
		// 这两句代码必须的，为的是初始化出来gson这个对象，才能拿来用
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		User resourcebeans = new User();
		if (datapart != "") {
			try {
				/*
				 * datapart="["+datapart+"]"; JSONArray objData =
				 * JSONArray.fromObject(datapart); //获取data字段
				 * resourcebeans=(User)JSONArray.toCollection(objData,
				 * User.class);
				 */
				JSONObject jsonObject = JSONObject.fromObject(datapart);
				java.lang.reflect.Type type0 = new TypeToken<User>() {
				}.getType();
				String str = jsonObject.toString();
				resourcebeans = gson.fromJson(str, type0);
			} catch (Exception e) {
				System.out.println("请求出现异常！" + e);
				e.printStackTrace();
			}
		}
		return resourcebeans;
	}

	/**
	 * 获取数据
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<User> parase(String datapart) {
		Gson gson = new Gson();
		// 这两句代码必须的，为的是初始化出来gson这个对象，才能拿来用
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		List<User> resourcebeans = new ArrayList<User>();
		if (datapart != "") {
			try {
				/*
				 * datapart="["+datapart+"]"; JSONArray objData =
				 * JSONArray.fromObject(datapart); //获取data字段
				 * resourcebeans=(List<User>)JSONArray.toCollection(objData,
				 * User.class);
				 */
				JSONObject jsonObject = JSONObject.fromObject(datapart);
				java.lang.reflect.Type type1 = new TypeToken<List<User>>() {
				}.getType();
				String str = jsonObject.toString();
				resourcebeans = gson.fromJson(str, type1);
			} catch (Exception e) {
				System.out.println("请求出现异常！" + e);
				e.printStackTrace();
			}
		}
		return resourcebeans;
	}
}
