package com.mvp.api.parase;

import java.util.List;

import net.sf.json.JSONObject;
import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mvp.api.apimodel.ApiGoods;

@SuppressLint("NewApi")
public class ParaseGoods {

	/**
	 * 获取数据
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static ApiGoods paraseGoods(String datapart) {
		Gson gson = new Gson();
		// 这两句代码必须的，为的是初始化出来gson这个对象，才能拿来用
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		ApiGoods resourcebeans = new ApiGoods();
		if (datapart != "") {
			try {
				/*
				 * datapart="["+datapart+"]"; JSONArray objData =
				 * JSONArray.fromObject(datapart); //获取data字段
				 * resourcebeans=(User)JSONArray.toCollection(objData,
				 * User.class);
				 */
				//此时不会带有[]
				JSONObject jsonObject = JSONObject.fromObject(datapart);
				java.lang.reflect.Type type0 = new TypeToken<ApiGoods>() {
				}.getType();
				String str = jsonObject.toString();
				resourcebeans = gson.fromJson(str, type0);
			} catch (Exception e) {
				System.out.println("请求出现异常！" + e);
				e.printStackTrace();
			}
		}
		return resourcebeans;
	}

	/**
	 * 获取数据
	 * 字母首写为小写
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<ApiGoods> paraseGoodsList(String datapart) {
		Gson gson = new Gson();
		// 这两句代码必须的，为的是初始化出来gson这个对象，才能拿来用
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
		List<ApiGoods> resourcebeans = null;// new ArrayList<ApiGoods>();
		if (!datapart.isEmpty()) {
			try {

				// datapart = "[" + datapart + "]";
				/*
				 * JSONArray objData = JSONArray.fromObject(datapart); //
				 * 获取data字段 resourcebeans = (List<ApiGoods>)
				 * JSONArray.toCollection(objData, ApiGoods.class);
				 */
				
				//此时会带有[]会解析不成功
				// datapart = datapart.replace("[", "");
				// datapart = datapart.replace("]", "");
				// JSONObject jsonObject = JSONObject.fromObject(datapart);
				java.lang.reflect.Type type1 = new TypeToken<List<ApiGoods>>() {
				}.getType();
				String str = datapart;// gson.toJson(jsonObject);
				resourcebeans = gson.fromJson(str, type1);
			} catch (Exception e) {
				System.out.println("请求出现异常！" + e);
				e.printStackTrace();
			}
		}
		return resourcebeans;
	}

	
}
