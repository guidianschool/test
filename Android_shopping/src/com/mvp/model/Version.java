package com.mvp.model;

public class Version {
	public int id;
	//服务器版本号
	public int version;
	//更新app名称
	public String name;
	//更新链接
	public String url;
	//更新内容
	public String content;
	//本地版本
	public int localVersion;

	public int getLocalVersion() {
		return localVersion;
	}

	public void setLocalVersion(int localVersion) {
		this.localVersion = localVersion;
	}

	public Version() {
	}

	public Version(int version, String name, String url) {
		super();
		this.version = version;
		this.name = name;
		this.url = url;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
