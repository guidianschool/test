package com.mvp.model.response;

import java.io.Serializable;

/***
 * 响应工具类
 * 
 * @author AlexLee
 */
public class ResponseData implements Serializable {

	// private static final long serialVersionUID = 1L;

	/**
	 * 返回状态码
	 */
	private int code;
	public int Code;
	/**
	 * 消息
	 */
	private String msg;
	public String Message;

	/**
	 * 返回数据
	 */
	private Object data;
	public Object Models;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public ResponseData() {

	}

	public ResponseData(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	/**
	 * 返回成功
	 *
	 * @return
	 */
	public static ResponseData ok() {
		return new ResponseData(200, "SUCCESS");
	}

	/**
	 * 返回成功
	 *
	 * @param data
	 *            返回数据
	 * @return 同一结果对象
	 */
	public static ResponseData ok(Object data) {
		return new ResponseData(data);
	}

	/**
	 * 返回失败
	 *
	 * @param code
	 *            状态码
	 * @param message
	 *            消息
	 * @return
	 */
	public static ResponseData fail(int code, String message) {
		return new ResponseData(code, message);
	}

	/**
	 * 返回失败
	 *
	 * @param message
	 *            消息
	 * @return
	 */
	public static ResponseData fail(String msg) {
		return new ResponseData(500, msg);
	}

	public ResponseData(Object data) {
		this(200, "SUCCESS");
		this.data = data;
	}
}
