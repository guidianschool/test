package com.mvp.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//用户表
public class User {
	int id;
	int sort;
	String loginName;
	String password;
	String name;
	int sex;
	String image;// 头像图片
	String registerTime;// 注册时间
	String birthday;// 生日
	String site;// 收货地址
	
	public User() {
	}

	public User(int iD, int sort,String loginName, String password,
			String name, int sex, String image, String registerTime,
			String birthday, String site) {
		super();
		this.loginName = loginName;
		this.password = password;
		this.id = iD;
		this.sort = sort;
		this.name = name;
		this.sex = sex;
		this.image = image;
		this.registerTime = registerTime;
		this.birthday = birthday;
		this.site = site;
	}



	// ***************公共方法***************************

	// 查询单个记录
	public User selectUserByID(List<User> list, int id) {
		User l = new User();
		for (User user : list) {
			if (user.id == id) {
				l = user;
				break;
			}
		}
		return l;
	}

	// 排序通过属性排序
	public List<User> sortUserListBySort(List<User> list) {
		Comparator<User> comparator = new Comparator<User>() {
			@Override
			public int compare(User s1, User s2) {
				// 先排序号
				if (s1.sort != s2.sort) {
					return s1.sort - s2.sort;
				} else {
					// 年龄相同则按姓名排序
					if (!s1.name.equals(s2.name)) {
						return s1.name.compareTo(s2.name);
					} else {
						// 姓名也相同则按学号排序
						return s1.id - s2.id;
					}
				}
			}
		};
		// 这里就会自动根据规则进行排序
		Collections.sort(list, comparator);
		return list;
	}

	// 删除一条记录
	public boolean deleteUserByID(List<User> list, int id) {
		boolean b = false;
		User l = new User();
		for (User user : list) {
			if (user.id == id) {
				l = user;
				b = true;
				break;
			}
		}
		list.remove(l);
		return b;
	}

	// 修改一条记录通过id
	public boolean updateUserByID(List<User> list, int id, String name) {
		boolean b = false;
		for (User user : list) {
			if (user.id == id) {
				user.name = name;
				b = true;
				break;
			}
		}
		return b;
	}

	// 获取用户名
	public Object getName() {
		// TODO Auto-generated method stub
		return "admin";
	}
	
	// 获取密码
	public String getPassword() {
		return "123";
	}

}
